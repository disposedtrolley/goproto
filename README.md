# goproto

Learning how to use protobufs with Go, following the
[official guide](https://developers.google.com/protocol-buffers/docs/gotutorial).

Prerequisites:

- Go
- `protoc` compiler: `brew install protobf`
- Protobuf plugin for Go: `go install
google.golang.org/protobuf/cmd/protoc-gen-go@latest`

With the prerequisites installed:

```command
# generate the Go package for encoding and decoding the protobuf.
$ protoc --go_out=. addressbook.proto

# run the Go program, which will encode and write an address book to
# `./book.out`, and then decode its contents into stdout
$ go run main.go
```