package main

import (
	"fmt"
	"io/ioutil"
	"log"

	"github.com/disposedtrolley/goproto/addressbookpb"
	"google.golang.org/protobuf/proto"
)

func writeBook(fname string, book *addressbookpb.AddressBook) {
	out, err := proto.Marshal(book)
	if err != nil {
		log.Fatalln("Failed to encode address book:", err)
	}
	if err := ioutil.WriteFile(fname, out, 0644); err != nil {
		log.Fatalln("Failed to write address book:", err)
	}
}

func readBook(fname string) *addressbookpb.AddressBook {
	in, err := ioutil.ReadFile(fname)
	if err != nil {
		log.Fatalln("Error reading file:", err)
	}
	book := &addressbookpb.AddressBook{}
	if err := proto.Unmarshal(in, book); err != nil {
		log.Fatalln("Failed to parse address book:", err)
	}

	return book
}

func main() {
	p := addressbookpb.Person{
		Id:    1234,
		Name:  "John Doe",
		Email: "jdoe@example.com",
		Phones: []*addressbookpb.Person_PhoneNumber{
			{Number: "555-4321", Type: addressbookpb.Person_HOME},
		},
	}

	book := &addressbookpb.AddressBook{
		People: []*addressbookpb.Person{&p},
	}

	writeBook("./book.out", book)

	fmt.Printf("%v\n", readBook("./book.out"))
}
